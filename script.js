/*
var or no var?
all hovers at start
make color list, replace switch color part?
treating numbers and strings etc confusion
*/

const r = 2.5; //capital letter constants?

const s3 = Math.sqrt(3);
const namespaceURI = "http://www.w3.org/2000/svg";

var svg = document.documentElement;

for (var i = 0; i < 17; i++) {
	var _i = i < 4 || (i > 8 && i < 13) ? i : 16-i;
	for (var j = 0; j <= _i; j++) {
		var c = document.createElementNS(namespaceURI, "circle");
		c.setAttribute("cx", (-_i+2*j)*2*r/s3);
		c.setAttribute("cy", 2*r*i);
		c.setAttribute("r", r);
		//c.setAttribute("style", "strokeWidth=0;");
		c.setAttribute("id", (i+1)+","+(j+(i-_i)/2+5));
		c.setAttribute("onclick", "f(this.id);");
		svg.appendChild(c);
	};
};

/*const B = [[9, 9, 9, 9, 9, 9],
[9, 9, 9, 9, 9, 1, 9],
[9, 9, 9, 9, 9, 1, 1, 9],
[9, 9, 9, 9, 9, 1, 1, 1, 9],
[9, 9, 9, 9, 9, 1, 1, 1, 1, 9, 9, 9, 9, 9],
[9, 6, 6, 6, 6, 0, 0, 0, 0, 0, 2, 2, 2, 2, 9],
[9, 9, 6, 6, 6, 0, 0, 0, 0, 0, 0, 2, 2, 2, 9],
[9, 9, 9, 6, 6, 0, 0, 0, 0, 0, 0, 0, 2, 2, 9],
[9, 9, 9, 9, 6, 0, 0, 0, 0, 0, 0, 0, 0, 2, 9],
[9, 9, 9, 9, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9],
[9, 9, 9, 9, 9, 5, 0, 0, 0, 0, 0, 0, 0, 0, 3, 9],
[9, 9, 9, 9, 9, 5, 5, 0, 0, 0, 0, 0, 0, 0, 3, 3, 9],
[9, 9, 9, 9, 9, 5, 5, 5, 0, 0, 0, 0, 0, 0, 3, 3, 3, 9],
[9, 9, 9, 9, 9, 5, 5, 5, 5, 0, 0, 0, 0, 0, 3, 3, 3, 3, 9],
[9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 4, 4, 4, 4, 9, 9, 9, 9, 9],
[9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 4, 4, 4, 9],
[9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 4, 4, 9],
[9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 4, 9],
[9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9]];*/

//wait there's no list index out of range error? what else have I missed?
const BS = "1111111111666600000222266600000022266000000022600000000200000000050000000035500000003355500000033355550000033334444444444";

var b;

function set(bs = BS) {
	/*b = B.map(function(row) {
	    return row.slice();
	});*/
	b = Array.from(Array(19), () => Array()); // https://stackoverflow.com/a/49201210
	var h = 0;
	for (var i = 0; i < 17; i++) {
		var _i = i < 4 || (i > 8 && i < 13) ? i : 16-i;
		for (var j = 0; j <= _i; j++) {
			b[i+1][j+(i-_i)/2+5] = +bs[h];
			h++;
		};
	};
	//debugger;
	path = [];
	for (var i = 0; i < b.length; i++) {
		var row = b[i];
		for (var j = 0; j < row.length; j++) {
			var p = b[i][j]; //should I use the letter p more?
			if (p < 7) {
				var color = "";
				switch (p) {
					case 1:
						color = "white";
						break;
					case 2:
						color = "yellow";
						break;
					case 3:
						color = "red";
						break;
					case 4:
						color = "black";
						break;
					case 5:
						color = "blue";
						break;
					case 6:
						color = "green";
						break;
				};
				chg(document.getElementById(i+","+j), color);
			};
		};
	};
	s = null;
};

var s = null; //selected

function f(id) { //messy
	console.log("CLICKED", id);
	var c = document.getElementById(id);
	id = id.split(",");
	if (c == s) {
		dsl(s);
		s = null;
		clear();
	} else {
		if (s) { //indent pattern bad?
			var p = b[+id[0]][+id[1]];
			if (p > 0 && p < 7) {			
				dsl(s);
			} else if (p == 7 || a) {
				chg(c, s.style.fill);
				//debugger;
				dsl(s); //only using the id and b parts
				chg(s);
				var sid = s.id.split(",");
				b[+id[0]][+id[1]] = b[+sid[0]][+sid[1]];
				b[+sid[0]][+sid[1]] = 0;
				s = null;
			};
		};
		if (c.style.fill) { //make it optional whether this is an if or an else if
			sel(c);
			s = c;
			clear(s);
			find(+id[0], +id[1], true);
			//console.log(path);
			/*for (var i = 0; i < path.length; i++) { //MAKE THIS PART OF FIND BUT ONLY RUN ONCE
				var pid = path[i];
				// var n = document.getElementById(pid[0]+","+pid[1]);
				// n.style.fill = "#87ADDE";
				chg(document.getElementById(pid[0]+","+pid[1]), "#87ADDE");
			};*/
		};
	};
};

function chg(c, color = "") {//a piece, etc.
	c.style.fill = color;
	c.style.stroke = color;
	c.style.strokeWidth = color ? "" : 0; //possible bug? sometimes still hoverable?
};

function sel(c) {
	c.style.stroke = "#87ADDE";
	c.style.strokeWidth = "1";
	var id = c.id.split(",");
	b[+id[0]][+id[1]] += 10;
};

function dsl(c) { //add s = null here?
	c.style.stroke = c.style.fill;
	c.style.strokeWidth = "";
	var id = c.id.split(",");
	b[+id[0]][+id[1]] -= 10;
};

var path = []; //append stuff to path to avoid repeating, and help visual display

function find(i, j, ff = false) { //rename ff (first first)
	if (!a) {
		var p = b[i][j];
		//console.log("p", p);
		var first = ff ? -1 : 1;
		[[-1, -1], [0, -1], [1, 0], [1, 1], [0, 1], [-1, 0]].forEach(function(d) {
			jump(i, j, d[0], d[1], 0, first); //tiit = 0
		});
	} else {
		for (var i = 0; i < b.length; i++) {
			var row = b[i];
			for (var j = 0; j < row.length; j++) {
				var p = b[i][j];
				if (p == 0) {
					path.push([i, j]);
					b[i][j] = 7;
				};
			};
		};
	};
	if (ff) {
		for (var i = 0; i < path.length; i++) { //MAKE THIS PART OF FIND BUT ONLY RUN ONCE
			var pid = path[i];
			// var n = document.getElementById(pid[0]+","+pid[1]);
			// n.style.fill = "#87ADDE";
			chg(document.getElementById(pid[0]+","+pid[1]), "#87ADDE");
		};
	};
};

function jump(i, j, di, dj, tiit = 0, first = 0) { //MAKE SPECIAL CASE FOR 14 4 0 jump, works in all types
	//if (first == -1 && tii) tiit += 1;
	i += di;
	j += dj;
	var p = b[i][j];
	//console.log(p);
	if (tii && tiit !== false) {
		if (tiit == 0 && first == 0 && p == 0) { //-1=first first; 0=not first; 1=recursive firsts //change to !x?
			path.push([i, j]);
			b[i][j] = 7;
			//if (!first) find(i, j);
			find(i, j);
		} else if (p == 0) {
			jump(i, j, di, dj, tiit+1);
		} else if (p > 0 && p < 7 && tiit > 0) { //including 7?
			jump(i, j, di, dj, -tiit);
		};
	};
	
	if (p > 0 && p < 7 && (ti || first) && !tii) { //disable this for tiit?
		jump(i, j, di, dj, false); //false necess?
	} else if (p == 0 && first < 1 && !tii) { //-1=first first; 0=not first; 1=recursive firsts
		path.push([i, j]);
		b[i][j] = 7;
		if (!first) find(i, j);
	};
	
};

function clear(except) {
	while (path.length > 0) {
		var pid = path.pop();
		var c = document.getElementById(pid[0]+","+pid[1]);
		if (c != except) {
			chg(c);
			b[pid[0]][pid[1]] = 0;
		};
	};
};

function port() { //enable port before set?
	if (s) {
		dsl(s);
		s = null;
		clear();
	};
	var bs = b.map(row => row.filter(p => p != 9).join("")).join("");
	var nbs = prompt("B.S.:", bs);
	if (nbs && nbs != bs) set(nbs); //save unnecessary step
};

var a = false;

function anarchy() {
	a = !a;
	document.getElementById("a").innerHTML = a ? "ANARCHY" : "Anarchy";
	if (s) {
		clear();
		var sid = s.id.split(",");
		find(+sid[0], +sid[1], true);
	};
};

var ti = true;

function typeI() { //please think of better name; combine with anarchy and others
	ti = !ti;
	document.getElementById("ti").innerHTML = ti ? "TYPE I" : "Type I";
	if (s) {
		clear();
		var sid = s.id.split(",");
		find(+sid[0], +sid[1], true);
	};
};

//type II is gonna be very problematic, and clash a lot with type I
//do type II true type I false first

var tii = false;

function typeII() {
	tii = !tii;
	document.getElementById("tii").innerHTML = tii ? "TYPE II" : "Type II";
	if (s) {
		clear();
		var sid = s.id.split(",");
		find(+sid[0], +sid[1], true);
	};
};

function button(c, fill) { //necessary?
	c.setAttribute("fill", fill);
};


set(); //disable if necessary